import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';
import {ExperienceCategoriesEntity} from "@entities/experience-categories.entity";
import {UserEntity} from "@entities/user.entity";

@Entity({
    name: 'experiences',
    orderBy: {
        createdAt: 'ASC',
    }
})
export class ExperienceEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({name: 'resume_or_work_sample_url', nullable: false})
    public resumeOrWorkSampleUrl: string;

    @Column({name: 'number_of_supervised_mentees', default: 0})
    public numberOfSupervisedMentees: number;

    @Column({name: 'project_sample', type: 'text'})
    public projectSample: string;

    @Column({name: 'project_development_description', type: 'text'})
    public projectDevelopmentDescription: string;

    @Column({name: 'career_level', default: 1})
    public careerLevel: number;

    @Column({name: 'giving_back_to_students_level', default: 1})
    public givingBackToStudents: number;

    @Column({name: 'growth_pedagogy_level', default: 1})
    public growthPedagogyLevel: number;

    @Column({name: 'branding_level', default: 1})
    public brandingLevel: number;

    @Column({name: 'info_source', comment: 'How did you find out about this opportunity?'})
    public informationSource: string;

    @Column({name: 'comment', nullable: true})
    public comment?: string;

    @OneToMany(() => ExperienceCategoriesEntity, object => object.experience, {eager: false, onDelete: 'CASCADE'})
    public categories: ExperienceCategoriesEntity[];

    @OneToMany(() => UserEntity, object => object.expertise, {eager: false, onDelete: 'CASCADE'})
    public user: UserEntity;

    @CreateDateColumn({name: 'created_at'})
    createdAt: Date;

    @UpdateDateColumn({name: 'updated_at'})
    updatedAt: Date;

    constructor(
        params?: Partial<ExperienceEntity>,
    ) {
        super();
        if (params) {
            Object.assign(this, params);
        }
    }
}
