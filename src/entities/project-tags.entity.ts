import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

@Entity({
    name: 'project_tags',
    orderBy: {
        createdAt: 'ASC',
    }
})
export class ProjectTagsEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({})
    public title: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}
