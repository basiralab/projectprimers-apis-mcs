import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';
import {ExperienceCategoriesEntity} from "@entities/experience-categories.entity";

@Entity({
    name: 'store_categories',
    orderBy: {
        createdAt: 'ASC',
    }
})
export class CategoriesStoreEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({name: 'slug', nullable: false})
    public slug: string;

    @Column({name: 'label', nullable: false})
    public label: string;

    @OneToMany(() => ExperienceCategoriesEntity, object => object.category, {eager: false, onDelete: 'CASCADE'})
    public experiences: ExperienceCategoriesEntity[];

    @CreateDateColumn({name: 'created_at'})
    createdAt: Date;

    @UpdateDateColumn({name: 'updated_at'})
    updatedAt: Date;
}
