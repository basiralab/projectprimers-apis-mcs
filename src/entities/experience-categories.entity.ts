import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';
import {ExperienceEntity} from "@entities/experience.entity";
import {CategoriesStoreEntity} from "@entities/categories-store.entity";

@Entity({
    name: 'experience_categories',
    orderBy: {
        createdAt: 'ASC',
    }
})
export class ExperienceCategoriesEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({name: 'slug', nullable: false})
    public slug: string;

    @Column({name: 'label', nullable: false})
    public label: string;

    @ManyToOne(() => ExperienceEntity, object => object.categories, {onDelete: "CASCADE"})
    @JoinColumn({name: 'experience_id'})
    public experience: ExperienceEntity;

    @ManyToOne(() => CategoriesStoreEntity, object => object.experiences, {onDelete: "CASCADE"})
    @JoinColumn({name: 'category_id'})
    public category: CategoriesStoreEntity;

    @CreateDateColumn({name: 'created_at'})
    createdAt: Date;

    @UpdateDateColumn({name: 'updated_at'})
    updatedAt: Date;
}
