import {BaseEntity, Column, CreateDateColumn, Entity, Index, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

@Entity({
    name: 'projects',
    orderBy: {
        createdAt: 'ASC',
    }
})
export class ProjectEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public slug?: string;

    @Column()
    public title: string;

    @Column({type: 'jsonb'})
    public tags?: string[];

    @Column({name: 'owner_id', unsigned: true, type: 'int4'})
    @Index('FK_PROJECT_OWNER_ID')
    public createdBy: number;

    @CreateDateColumn({name: 'created_at'})
    createdAt: Date;

    @UpdateDateColumn({name: 'updated_at'})
    updatedAt: Date;
}
