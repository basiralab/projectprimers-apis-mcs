import {UserAccountStatusEnum, UserRolesEnum} from '@helpers/constants';
import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    Unique,
    UpdateDateColumn
} from 'typeorm';
import {ExperienceEntity} from "@entities/experience.entity";

@Entity({
    name: 'users',
    orderBy: {
        createdAt: 'ASC',
    }
})
@Unique('UK_EMAIL', ['email'])
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({name: 'first_name', nullable: true})
    public firstName?: string;

    @Column({name: 'last_name', nullable: false})
    public lastName: string;

    @Column({name: 'email', nullable: false})
    public email: string;

    @Column({name: 'job_title', nullable: true})
    public jobTitle: string;

    @Column({name: 'company', nullable: true})
    public company: string;

    @Column({name: 'website', nullable: true})
    public website?: string;

    @Column({name: 'country', nullable: false})
    public country: string;

    @Column({name: 'linked_in_url', nullable: true})
    public linkedInUrl?: string;

    @Column({name: 'github_url', nullable: true})
    public gitHubUrl?: string;

    @Column({name: 'twitter_url', nullable: true})
    public twitterUrl?: string;

    @Column({type: 'jsonb', default: [UserRolesEnum.MENTEE]})
    public roles: UserRolesEnum[];

    @Column({nullable: true})
    public salt: string;

    @Column({nullable: true})
    public password: string;

    @Column({type: "enum", enum: UserAccountStatusEnum, default: UserAccountStatusEnum.INACTIVE})
    public accountStatus: UserAccountStatusEnum;

    @Column({type: 'timestamptz', nullable: true})
    public activatedAt?: Date;

    @ManyToOne(() => ExperienceEntity, object => object.id, {onDelete: "CASCADE", eager: true})
    @JoinColumn({name: 'experience_id'})
    public expertise: ExperienceEntity;

    @CreateDateColumn({name: 'created_at'})
    createdAt: Date;

    @UpdateDateColumn({name: 'updated_at'})
    updatedAt: Date;
}
