import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {ConfigService} from "@nestjs/config";
import {HttpExceptionFilter} from "@exceptions/http-exceptions-filters";
import {QueryExceptionsFilters} from "@exceptions/query-exceptions-filters";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);
  app.enableCors({
    origin: '*',
  });

  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalFilters(new QueryExceptionsFilters());

  await app.listen(config.get('USERS_MCS_PORT') || 3000, '0.0.0.0');
}
bootstrap();
