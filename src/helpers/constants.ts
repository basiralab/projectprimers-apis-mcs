export enum UserRolesEnum {
    ADMIN = 'admin',
    MENTEE = 'mentee',
    MENTOR = 'mentor',
}

export enum UserAccountStatusEnum {
    ENABLED = 'enabled',
    DISABLED = 'disabled',
    INACTIVE = 'inactive',
}
