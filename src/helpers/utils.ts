import {ConfigService} from "@nestjs/config";
// import path from "path";
// import fs from "fs";


export const getJwtKeys = async (
    cnf: ConfigService,
) => {
    const keyFileName = `${cnf.get('JWT_KEYS_FILE_NAME')}.key`;
    const path = await import('path');
    const fs = await import('fs');

    let privateKey, publicKey;
    const keysDir = path.join(__dirname, `../../keys`);

    if (
        fs.existsSync(keysDir + '/' + keyFileName) &&
        fs.existsSync(keysDir + '/' + keyFileName + '.pub')
    ) {
        privateKey = fs.readFileSync(keysDir + '/' + keyFileName, 'utf8')?.replace(/\\n/gm, '\n');
        publicKey = fs.readFileSync(keysDir + '/' + keyFileName + '.pub', 'utf8')?.replace(/\\n/gm, '\n');
    } else {
        // logger.error('Keys does not exist');
        // TODO throw error if not exists
    }

    return {privateKey, publicKey};
}

/**
 * String to Base64
 * */
export const generateToken = (str: string): string => {
    return Buffer.from(str).toString("base64");
}

/**
 * Base64 to Json
 * */
export const parseToken = (str: string): string => {
    return new Buffer(str, 'base64').toString('ascii');
}