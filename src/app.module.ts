import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from '@modules/auth/auth.module';
import {TypeOrmModule, TypeOrmModuleOptions} from "@nestjs/typeorm";
import { UsersModule } from '@modules/users/users.module';
import { CategoriesModule } from '@modules/categories/categories.module';
import { ExperienceModule } from '@modules/experience/experience.module';
import {BullModule} from "@nestjs/bull";
import { EmailsModule } from '@modules/emails/emails.module';
import {ProjectsModule} from "@modules/projects/projects.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        const env = config.get('ENV') || 'local';
        // tslint:disable-next-line:no-console
        console.log('*************** Server running on Port : ' + config.get('USERS_MCS_PORT') + ' on ' + env + ' Environment *********************');
        return {
          database: config.get('DB_NAME'),
          host: config.get('DB_HOST'),
          password: config.get('DB_PASSWORD'),
          username: config.get('DB_USER'),
          entities: ['dist/**/*.entity.js'],
          // subscribers: ['dist/subscribers/*.subscriber.js'],
          synchronize: false, // prevent crash on local environment, TODO: environment aware value
          type: 'postgres',
          port: +config.get('DB_PORT'),
          // dropSchema: true,
          // logging: true,
          // migrationsTableName: 'migrations',
          // migrations: ['migration/*.js'],
          // cli: {
          //   migrationsDir: 'migration',
          // },
        } as TypeOrmModuleOptions;
      },
    }),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          prefix: 'pp_',
          redis: {
            host: config.get('REDIS_HOST'),
            port: config.get('REDIS_PORT'),
            password: config.get('REDIS_PASSWORD'),
          }
        }
      },
    }),
    AuthModule,
    UsersModule,
    CategoriesModule,
    ExperienceModule,
    EmailsModule,
    ProjectsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
