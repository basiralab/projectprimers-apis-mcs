import {Module} from '@nestjs/common';
import {ProjectsController} from './projects.controller';
import {ProjectsService} from './projects.service';
// import {ProjectEntity} from "../../entities/project.entity";
// import {ExpressCassandraModule} from "@iaminfinity/express-cassandra";
// import {ProjectTagsEntity} from "../../entities/project-tags.entity";

@Module({
  controllers: [ProjectsController],
  providers: [ProjectsService],
  imports: [
    /*ExpressCassandraModule.forFeature([
      ProjectEntity,
      ProjectTagsEntity
    ]),*/
  ],
  exports: [/*ExpressCassandraModule*/],
})
export class ProjectsModule {}
