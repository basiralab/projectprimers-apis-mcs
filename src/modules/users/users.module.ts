import {Module} from '@nestjs/common';
import {UsersController} from './users.controller';
import {UsersService} from './users.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {CategoriesModule} from "@modules/categories/categories.module";
import {ExperienceModule} from "@modules/experience/experience.module";

@Module({
    controllers: [UsersController],
    providers: [UsersService],
    imports: [
        // AuthModule,
        CategoriesModule,
        ExperienceModule,
        TypeOrmModule.forFeature([

        ]),
    ],
    exports: [
        TypeOrmModule,
        UsersService,
        ExperienceModule,
        CategoriesModule,
    ],
})
export class UsersModule {}
