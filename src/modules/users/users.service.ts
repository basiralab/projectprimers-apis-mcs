import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {UserEntity} from "@entities/user.entity";
import {Repository} from "typeorm";
import {ExperienceEntity} from "@entities/experience.entity";
import {CategoriesService} from "@modules/categories/categories.service";

@Injectable()
export class UsersService {
    constructor(
       @InjectRepository(UserEntity)
       private usersRepository: Repository<UserEntity>,
       @InjectRepository(ExperienceEntity)
       private experienceRepository: Repository<ExperienceEntity>,

       private readonly categoriesService: CategoriesService,
    ) {}

    public getRepository(): Repository<UserEntity> {
        return this.usersRepository;
    }

    public getExperiencesRepository(): Repository<ExperienceEntity> {
        return this.experienceRepository;
    }
}
