import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {CategoriesStoreEntity} from "@entities/categories-store.entity";
import {ExperienceCategoriesEntity} from "@entities/experience-categories.entity";

@Injectable()
export class CategoriesService {
    constructor(
        @InjectRepository(CategoriesStoreEntity)
        private categoriesRepository: Repository<CategoriesStoreEntity>,
        @InjectRepository(ExperienceCategoriesEntity)
        private experienceCategoriesRepository: Repository<ExperienceCategoriesEntity>,
    ) {}

    public getRepository(): Repository<CategoriesStoreEntity> {
        return this.categoriesRepository;
    }

    public getExperienceCategoriesRepository(): Repository<ExperienceCategoriesEntity> {
        return this.experienceCategoriesRepository;
    }
}
