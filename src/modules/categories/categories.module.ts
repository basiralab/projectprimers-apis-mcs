import { Module } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CategoriesController } from './categories.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {CategoriesStoreEntity} from "@entities/categories-store.entity";
import {ExperienceCategoriesEntity} from "@entities/experience-categories.entity";

@Module({
  providers: [CategoriesService],
  controllers: [CategoriesController],
  imports: [
    TypeOrmModule.forFeature([
        CategoriesStoreEntity,
        ExperienceCategoriesEntity,
    ])
  ],
  exports: [
      TypeOrmModule,
      CategoriesService,
  ]
})
export class CategoriesModule {}
