import {Injectable, NotFoundException,} from '@nestjs/common';
import {AuthService} from './auth.service';
import {ConfigService} from '@nestjs/config';
import {PassportStrategy} from '@nestjs/passport';
import {ExtractJwt, Strategy} from 'passport-jwt';
import {UserEntity} from "../../entities/user.entity";
import {getJwtKeys} from "@helpers/utils";
import {JwtService} from "@nestjs/jwt";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
      private authService: AuthService,
      private CONFIG: ConfigService,
      private jwtService: JwtService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      algorithms: ['RS256'],
      secretOrKeyProvider: async (requestType, token, done) => {
        const {privateKey} = await getJwtKeys(this.CONFIG);
        done(undefined, privateKey);
      },
      ignoreExpiration: false,
    });
  }

  public async validate({ id }: { id: number }): Promise<UserEntity> {
    try {
      return await this.authService.getRepository().findOne({id});
    } catch (err) {
      throw new NotFoundException();
    }
  }
}
