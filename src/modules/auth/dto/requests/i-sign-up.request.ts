export interface ISignUpRequest {
    firstName?: string;
    lastName: string;
    email: string;
    password: string;
    jobTitle: string;
    company: string;
    website?: string;
    linkedInUrl?: string;
    gitHubUrl?: string;
    twitterUrl?: string;
    country: string;
    experience: IUserExperience;
}

export interface IUserExperience {
    resumeOrWorkSampleUrl: string;
    projectSample: string;
    projectDevelopmentDescription: string;
    informationSource: string;
    comment: string;
    numberOfSupervisedMentees: number;
    careerLevel: number;
    givingBackToStudents: number;
    growthPedagogyLevel: number;
    brandingLevel: number;
}
