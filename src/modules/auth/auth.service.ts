import {ConflictException, ForbiddenException, Injectable, NotFoundException, OnModuleInit} from '@nestjs/common';
import {JwtService} from "@nestjs/jwt";
import {InjectRepository} from "@nestjs/typeorm";
import * as bcrypt from 'bcrypt';
import {UserEntity} from "@entities/user.entity";
import {Repository} from "typeorm";
import {ISignUpRequest} from "@modules/auth/dto/requests/i-sign-up.request";
import {Request} from "express";
import {ISignInRequest} from "@modules/auth/dto/requests/i-sign-in.request";
import {UserAccountStatusEnum, UserRolesEnum} from "@helpers/constants";
import {Queue} from "bull";
import {InjectQueue} from "@nestjs/bull";
import {omit} from "lodash";
import {ExperienceService} from "@modules/experience/experience.service";
import {ExperienceEntity} from "@entities/experience.entity";

@Injectable()
export class AuthService implements OnModuleInit {
    constructor(
        @InjectQueue(process.env.REDIS_SIGNUP_EMAILS_QUEUE)
        private readonly signUpQueue: Queue,
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        private jwtService: JwtService,
        private experienceService: ExperienceService,
    ) {}

    public getRepository(): Repository<UserEntity> {
        return this.userRepository;
    }

    public async signUp(
        params: ISignUpRequest,
        req?: Request,
    ): Promise<UserEntity> {
        const check = await this.userRepository.findOne({email: params.email});
        if (check) {
            throw new ConflictException('email_taken');
        }

        const experiense = await this.experienceService.getRepository().save(
            new ExperienceEntity(params.experience),
        );

        const salt = await bcrypt.genSalt();

        const user = await this.userRepository.save({
            ...omit(params, ['experience']),
            salt,
            password: await AuthService.hashPassword(params.password, salt),
            roles: [UserRolesEnum.MENTEE],
            expertise: experiense,
        }, {reload: true});

        await this.signUpQueue.add(process.env.REDIS_SIGNUP_EMAILS_QUEUE,{user});

        return user;
    }

    public async signIn(
        params: ISignInRequest,
        req?: Request,
    ): Promise<{ access_token: string, decoded?: any }> {
        const user = await this.userRepository.findOne({email: params.email});
        if (!user) {
            throw new NotFoundException('user_not_found');
        }

        if (user.accountStatus === UserAccountStatusEnum.INACTIVE) {
            throw new ForbiddenException('account_not_activated');
        }

        if (user.accountStatus === UserAccountStatusEnum.DISABLED) {
            throw new ForbiddenException('account_disabled');
        }

        const checkCredentials = await AuthService.validatePassword(params.password, user.salt, user.password);

        if (!checkCredentials) {
            throw new NotFoundException('user_not_found');
        }
        const token = this.jwtService.sign({
            id: user.id,
            iss: process.env.APP_NAME
        });

        // const decoded = this.jwtService.decode(token);
        return {
            access_token: token,
            decoded: {
                userId: user.id,
            },
        };
    }

    public async getMe(
        user: UserEntity,
    ) {
        return user;
    }

    async onModuleInit() {

        /*setTimeout(async () => {
            const x = await this.jwtService.sign({email: 'test@test.com', iss: 'Me'});
            // const y = await this.jwtService.verify(x);
            console.log(x);
        }, 3000);
        await this.userRepository.save({
            email: 'tests'+ Math.random() * 100 + '@test.com',
            lastName: 'test',
            roles: ['admin'],
        });*/
    }

    private static async validatePassword(
        pwd: string,
        salt: string,
        hashPwd: string,
    ): Promise<boolean> {
        const hash = await bcrypt.hash(pwd, salt);
        return hash === hashPwd;
    }

    private static hashPassword(
        pwd: string,
        salt: string,
    ): Promise<string> {
        return bcrypt.hash(pwd, salt);
    }
}
