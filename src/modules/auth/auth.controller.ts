import {Body, Controller, Get, HttpStatus, Post, Req, UseGuards} from '@nestjs/common';
import {AuthService} from "./auth.service";
import {Request} from "express";
import {ISignUpRequest} from "@modules/auth/dto/requests/i-sign-up.request";
import {AuthGuard} from "@nestjs/passport";
import {UserEntity} from "@entities/user.entity";

@Controller('auth')
export class AuthController {
    constructor(
        private readonly service: AuthService,
    ) {}

    @Post('/sign-up')
    public signUp(
        @Req() req: Request,
        @Body() params: ISignUpRequest,
    ) {
        return this.service.signUp(params, req);
    }

    @Post('/token')
    public signIn(
        @Req() req: Request,
        @Body() params: ISignUpRequest,
    ): Promise<any> {
        return this.service.signIn(params, req);
    }

    @Post('/verify')
    @UseGuards(AuthGuard('jwt'))
    public verifyToken(
        @Req() req: Request,
    ): {status: HttpStatus} {
        // JWT module does the job for us
        return {
            status: HttpStatus.OK,
        };
    }

    @Get('/me')
    @UseGuards(AuthGuard('jwt'))
    public me(
        @Req() req: Request,
    ): Promise<UserEntity> {
        // JWT module does the job for us
        return this.service.getMe(req.user as UserEntity);
    }
}
