import {Global, Module} from '@nestjs/common';
import {AuthController} from './auth.controller';
import {AuthService} from "./auth.service";
import {PassportModule} from '@nestjs/passport';
import {JwtModule, JwtSecretRequestType} from "@nestjs/jwt";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {JwtModuleOptions} from "@nestjs/jwt/dist/interfaces/jwt-module-options.interface";
import {JwtStrategy} from "./jwt.strategy";
import {TypeOrmModule} from "@nestjs/typeorm";
import {UserEntity} from "@entities/user.entity";
import {getJwtKeys} from "@helpers/utils";
import {UsersModule} from "@modules/users/users.module";

@Global()
@Module({
    controllers: [AuthController],
    imports: [
        TypeOrmModule.forFeature([
            UserEntity,
        ]),
        PassportModule.registerAsync({
            // inject: [ConfigService],
            useFactory: async () => {
                return {
                    defaultStrategy: 'jwt',
                };
            },
        }),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (cnf: ConfigService) => {
                // const logger = new Logger('JWT INIT');
                const {privateKey, publicKey} = await getJwtKeys(cnf);
                return {
                    privateKey,
                    publicKey,
                    // secret: cnf.get('JWT_SECRET'),
                    signOptions: {
                        expiresIn: cnf.get('JWT_EXPIRES_IN'),
                        algorithm: 'RS256',
                    },
                    verifyOptions: {
                        algorithms: ['RS256'],
                    },
                    secretOrKeyProvider: (requestType) => {
                        switch (requestType) {
                            case JwtSecretRequestType.SIGN:
                                // Private key
                                return privateKey;
                            case JwtSecretRequestType.VERIFY:
                                // Public key
                                return publicKey;
                            default:
                                return publicKey;
                        }
                    },
                } as JwtModuleOptions;
            }
        }),
        UsersModule,
    ],
    exports: [TypeOrmModule, JwtModule, PassportModule],
    providers: [
        AuthService,
        JwtStrategy,
    ],
})
export class AuthModule {}
