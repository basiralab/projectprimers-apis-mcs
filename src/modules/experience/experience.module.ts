import {Module} from '@nestjs/common';
import {ExperienceService} from './experience.service';
import {ExperienceController} from './experience.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ExperienceCategoriesEntity} from "@entities/experience-categories.entity";
import {ExperienceEntity} from "@entities/experience.entity";

@Module({
  providers: [ExperienceService],
  controllers: [ExperienceController],
  imports: [
      TypeOrmModule.forFeature([
          ExperienceCategoriesEntity,
          ExperienceEntity,
      ]),
  ],
  exports: [
      TypeOrmModule,
      ExperienceService,
  ]
})
export class ExperienceModule {}
