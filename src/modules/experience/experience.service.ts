import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {ExperienceEntity} from "@entities/experience.entity";
import {ExperienceCategoriesEntity} from "@entities/experience-categories.entity";

@Injectable()
export class ExperienceService {
    constructor(
        @InjectRepository(ExperienceEntity)
        private experienceRepository: Repository<ExperienceEntity>,
        @InjectRepository(ExperienceCategoriesEntity)
        private experienceCategoriesRepository: Repository<ExperienceCategoriesEntity>,
    ) {}

    public getRepository(): Repository<ExperienceEntity> {
        return this.experienceRepository;
    }

    public getExperienceCategoriesRepository(): Repository<ExperienceCategoriesEntity> {
        return this.experienceCategoriesRepository;
    }
}
