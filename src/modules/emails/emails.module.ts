import {Global, Module} from '@nestjs/common';
import {EmailsService} from './emails.service';
import {EmailsConsumer} from "@modules/emails/processors/emails.consumer";
import {BullModule} from "@nestjs/bull";
import {MailerModule} from "@nestjs-modules/mailer";
import {EjsAdapter} from "@nestjs-modules/mailer/dist/adapters/ejs.adapter";
import {ConfigModule, ConfigService} from "@nestjs/config";

@Global()
@Module({
  imports: [
    BullModule.registerQueue(
        {
          name: process.env.REDIS_SIGNUP_EMAILS_QUEUE
        }
    ),
    MailerModule.forRootAsync({
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (cnf: ConfigService) => ({
              transport: {
                  host: cnf.get('SMTP_HOST'),
                  port: +cnf.get('SMTP_PORT'),
                  secure: false, // upgrade later with STARTTLS
                  auth: {
                      user: cnf.get('SMTP_USER'),
                      pass: cnf.get('SMTP_PASSWORD'),
                  },
              },
              defaults: {
                  from: `${cnf.get('APP_NAME')} ${cnf.get('SMTP_FROM')}`,
              },
              // preview: true,
              template: {
                  dir: process.cwd() + '/templates/emails/',
                  adapter: new EjsAdapter(), // or new PugAdapter()
                  options: {
                      strict: false,
                  },
              },
          }),
  }),
  ],
  providers: [EmailsService, EmailsConsumer],
  exports: [EmailsConsumer, BullModule],
})
export class EmailsModule {}
