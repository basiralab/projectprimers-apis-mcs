export interface ISendEmailParam {
    subject: string;
    template: string;
    context?: Record<string, any>,
}

type EMAIL_TYPES = 'USER_ACCOUNT_CREATED' | 'PASSWORD_RESET_LINK';

export const EmailTypesDefaults: Record<EMAIL_TYPES, ISendEmailParam> = {
    USER_ACCOUNT_CREATED: {
        subject: 'Your account has been created',
        template: 'account-activate',
    },
    PASSWORD_RESET_LINK: {
        subject: 'Password reset',
        template: 'password-reset',
    }
};
