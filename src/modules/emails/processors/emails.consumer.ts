import {Process, Processor} from "@nestjs/bull";
import {Logger, Scope} from "@nestjs/common";
import {EmailsService} from "@modules/emails/emails.service";
import {Job} from "bull";
import {EmailTypesDefaults} from "@modules/emails/email.settings";

@Processor({
    name: process.env.REDIS_SIGNUP_EMAILS_QUEUE,
    scope: Scope.REQUEST
})
export class EmailsConsumer {
    private readonly logger = new Logger(EmailsConsumer.name);

    constructor(
        private readonly emailService: EmailsService,
    ) {}

    @Process(process.env.REDIS_SIGNUP_EMAILS_QUEUE)
    public async handle(job: Job) {
        const {user} = job.data;
        this.logger.warn('✨[JOB] Sending email to ' + user.email);

        /*const payload: IToken = {
            requestedAt: moment().toDate(),
            expiresAt: moment()
                .add('1 month')
                .toDate(),
            randomSalt: (Math.random() * 99999).toString(),
            email,
        };
        const token = generateToken(JSON.stringify(payload));
        */

        try {
            const res = await this.emailService.send(
                user,
                {
                    ...EmailTypesDefaults.USER_ACCOUNT_CREATED,
                    context: {
                        token: 'XXXXXXXXXXXX',
                    }
                },
            );
            this.logger.warn('✨[JOB] Sending email to ' + user.email + ' completed');
        } catch (e) {
            this.logger.error('✨[JOB] Error Sending email to ' + user.email);
            this.logger.error(JSON.stringify(e));
        }
    }
}
