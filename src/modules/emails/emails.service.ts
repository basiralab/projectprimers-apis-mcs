import {Injectable} from '@nestjs/common';
import {MailerService} from "@nestjs-modules/mailer";
import {UserEntity} from "@entities/user.entity";
import {ISendEmailParam} from "@modules/emails/email.settings";

@Injectable()
export class EmailsService {
    constructor(
        private readonly mailerService: MailerService,
    ) {}

    public async send(
        to: Partial<UserEntity>,
        payload: ISendEmailParam,
    ): Promise<any> {
        const context = payload.context || {};
        Object.assign(
            context,
            {
                user: to,
                env: {
                    SERVER_URL: process.env.GATEWAY_API_HOST,
                    FRONT_URL: process.env.CLIENT_HOST,
                }
            }
        );

        return this.mailerService
            .sendMail({
                to: to.email,
                subject: payload.subject,
                template: payload.template,
                context,
            });
    }
}
